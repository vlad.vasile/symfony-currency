package magoffice.app;

import java.lang.management.ManagementFactory;
import java.lang.management.RuntimeMXBean;
import java.sql.SQLException;

import javax.annotation.PostConstruct;

import org.h2.tools.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.event.EventListener;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.EnumerablePropertySource;
import org.springframework.core.env.Environment;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.FilterType;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.annotation.PropertySources;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.data.repository.CrudRepository;

import com.fasterxml.jackson.databind.Module;

import io.vavr.collection.List;
import io.vavr.collection.Map;
import io.vavr.collection.TreeMap;
import io.vavr.jackson.datatype.VavrModule;
import lombok.extern.slf4j.Slf4j;
import reactor.core.publisher.Hooks;

@SpringBootApplication(scanBasePackages = { "magoffice.*" })
@EnableAutoConfiguration
@PropertySources({
    @PropertySource(value = "classpath:/magoffice.properties") })
//@ContextConfiguration(initializers = ConfigFileApplicationContextInitializer.class)
@Slf4j
public class CurrencyProviderApplication {
  @Autowired
  Environment env;
  @Autowired
  ApplicationContext applicationContext;

  public static void main(String[] args) {
    Hooks.onOperatorDebug();
    SpringApplication.run(CurrencyProviderApplication.class, args);
  }

  @PostConstruct
  public void init() throws Exception {
    showInfoProperties();
    showJVMParameters();
  }

  @Bean
  Module vavrModule() {
    return new VavrModule();
  }

  @EventListener(ApplicationReadyEvent.class)
  public void doSomethingAfterStartup() {
    log.info("hello world, I have just started up");
  }

  public static Map<String, Object> getAllKnownProperties(Environment env) {
    Map<String, Object> rtn = TreeMap.empty();
    if (env instanceof ConfigurableEnvironment) {
      for (org.springframework.core.env.PropertySource<?> propertySource : ((ConfigurableEnvironment) env)
        .getPropertySources()) {
        if (propertySource instanceof EnumerablePropertySource) {
          for (String key : ((EnumerablePropertySource) propertySource).getPropertyNames()) {
            rtn = rtn.put(key + "@" + propertySource.getName(), propertySource.getProperty(key));
          }
        }
      }
    }
    return rtn;
  }

  private void showInfoProperties() {
    log.info(
      "*********************Please check out this link if you need to override bellow application properties:\nhttps://docs.spring.io/spring-boot/docs/1.5.22.RELEASE/reference/html/boot-features-external-config.html\n\nProperties:\n"
          + getAllKnownProperties(env).map(x -> "    " + x._1() + "=" + x._2 + "\n").mkString()
          + "*********************\n");
    if (log.isDebugEnabled()) {
      log.debug("\n\n\nBeans:\n**********\nbean:"
          + List.of(applicationContext.getBeanDefinitionNames()).mkString("\nbean:"));
    }
  }

  private void showJVMParameters() {
    log.info(">>>>>>>>>>>>>>>>> JVM PARAMETERS <<<<<<<<<<<<<<<<<<<<<<<<<<");
    RuntimeMXBean runtimeMXBean = ManagementFactory.getRuntimeMXBean();
    java.util.List<String> jvmArgs = runtimeMXBean.getInputArguments();
    for (String arg : jvmArgs) {
      log.info(arg);
    }
    log.info(">>>>>>>>>>>>>>>>> JVM PARAMETERS END <<<<<<<<<<<<<<<<<<<<<<<<");
  }

}
