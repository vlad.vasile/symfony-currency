package magoffice.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@XmlRootElement(name = "Header")
@Getter(value = AccessLevel.NONE)
@Setter(value = AccessLevel.NONE)
public class Header {
  @XmlElement(name = "Publisher")
  public String publisher;
  @XmlElement(name = "PublishingDate")
  public String publishingDate;
  @XmlElement(name = "MessageType")
  public String messageType;
}
