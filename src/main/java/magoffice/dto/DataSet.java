package magoffice.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
//@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(name = "", propOrder = { "header", "body" })
@XmlRootElement(name = "DataSet")
@Getter(value = AccessLevel.NONE)
@Setter(value = AccessLevel.NONE)
public class DataSet {
  @XmlElement(name = "Body", nillable = false)
  public Body body;
  @XmlElement(name = "Header")
  public Header header;
}