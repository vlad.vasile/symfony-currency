package magoffice.dto;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlValue;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@XmlRootElement(name = "Rate", namespace = "http://www.bnr.ro/xsd")
@Getter(value = AccessLevel.NONE)
@Setter(value = AccessLevel.NONE)
public class CurrencyDto {
  @XmlAttribute(name = "currency")
  public String currencyName;
  @XmlAttribute(name = "multiplier")
  public String multiplier;
  @XmlValue
  public String rate;

}
