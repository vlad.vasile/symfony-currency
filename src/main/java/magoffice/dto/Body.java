package magoffice.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@XmlRootElement(name = "Body", namespace = "http://www.bnr.ro/xsd")
@Getter(value = AccessLevel.NONE)
@Setter(value = AccessLevel.NONE)
public class Body {
  @XmlElement(name = "Subject")
  public String subject;
  @XmlElement(name = "OrigCurrency")
  public String originalCurrency;
  @XmlElement(name = "Cube")
  public Cube ratesSection;
}
