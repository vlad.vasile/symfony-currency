package magoffice.dto;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement(namespace = "http://www.bnr.ro/xsd")
public class AllData {
  @XmlElement(name = "DataSet")
  public DataSet dataSet;
}
