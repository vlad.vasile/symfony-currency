package magoffice.dto;

import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;

import lombok.AccessLevel;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@XmlRootElement(name = "Cube", namespace = "http://www.bnr.ro/xsd")
@Getter(value = AccessLevel.NONE)
@Setter(value = AccessLevel.NONE)
public class Cube {
  @XmlAttribute(name = "date")
  public String date;
  @XmlElement(name = "Rate")
  public List<CurrencyDto> currencies = new ArrayList<CurrencyDto>();
}
