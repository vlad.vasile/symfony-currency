package magoffice.domain;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.OffsetDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.vavr.control.Either;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import magoffice.model.CurrencyInfoModel;
import magoffice.ui.common.CurrencyProviderService;
import magoffice.ui.works.CurrencyProviderController.CurrencyRequest;

@Slf4j
@Component
public class CurrencyProviderServiceImpl implements CurrencyProviderService {
  private static final int ROUNDING = 4;
  public final CurrenciesFeed currenciesFeed;

  @Autowired
  public CurrencyProviderServiceImpl(@Value("${currency.rates.interval}") Duration pullInterval,
      @Value("${currency.rates.url}") String ratesUrl)
  {
    log.info("Starting pulling currencies info... on a {} period at {}", pullInterval, ratesUrl);
    this.currenciesFeed = CurrenciesFeed.from(pullInterval, ratesUrl);
  }

  @Override
  public Either<CurrencyInfoModel, String> retrieveParity(CurrencyRequest currencyRequest) {
    if (!currenciesFeed.areCurrenciesAvailable(currencyRequest)) {
      String errorMessage = unavailableErrorMessage(currencyRequest);
      return Either.right(errorMessage);
    }

    String parity = computeParity(currencyRequest);

    return Either.left(CurrencyInfoModel.builder()
      .baseCurrency(currencyRequest.baseCurrency)
      .secondaryCurrency(currencyRequest.secondaryCurrency)
      .parity(parity)
      .rounding(ROUNDING)
      .requestDate(OffsetDateTime.now())
      .lastUpdate(currenciesFeed.lastUpdate)
      .build());

  }

  private String unavailableErrorMessage(CurrencyRequest currencyRequest) {
    return String.format("Parity [%s/%s] not available", currencyRequest.baseCurrency,
      currencyRequest.secondaryCurrency);
  }

  private String computeParity(CurrencyRequest currencyRequest) {
    Currency baseCurrency = currenciesFeed.getCurrency(currencyRequest.baseCurrency);
    BigDecimal baseCurrencyRate = baseCurrency.multiplyRate();

    Currency secondaryCurrency = currenciesFeed.getCurrency(currencyRequest.secondaryCurrency);
    BigDecimal secondaryCurrencyRate = secondaryCurrency.multiplyRate();

    RatesCalculator ratesCalculator = RatesCalculator.builder()
      .baseRate(baseCurrencyRate)
      .secondaryRate(secondaryCurrencyRate)
      .build();

    BigDecimal calculatedRate = ratesCalculator.divide();
    String parity = String.format("1 / %s", calculatedRate);
    log.info("Parity {}/{} : {}", currencyRequest.baseCurrency, currencyRequest.secondaryCurrency,
      parity);
    return parity;
  }

  @Data
  @Builder
  private static class RatesCalculator {
    private final BigDecimal baseRate;
    private final BigDecimal secondaryRate;

    public BigDecimal divide() {
      return secondaryRate.divide(baseRate, ROUNDING, RoundingMode.HALF_UP);
    }
  }

}
