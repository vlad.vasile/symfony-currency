package magoffice.domain;

import java.io.StringReader;
import java.math.BigDecimal;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;
import java.time.Duration;
import java.time.LocalDate;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.net.ssl.SSLContext;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;

import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

import lombok.extern.slf4j.Slf4j;
import magoffice.dto.CurrencyDto;
import magoffice.dto.DataSet;
import magoffice.ui.works.CurrencyProviderController.CurrencyRequest;
import reactor.core.publisher.Flux;

@Slf4j
public class CurrenciesFeed {
  private static final String RON_CURRENCY = "RON";
  private final String ratesUrl;
  public LocalDate lastUpdate;
  public Map<String, Currency> ratesBasedOnRonCurrency = new ConcurrentHashMap<String, Currency>();

  public static CurrenciesFeed from(Duration pullInterval, String ratesUrl) {
    return new CurrenciesFeed(pullInterval, ratesUrl);
  }

  public CurrenciesFeed(Duration pullInterval, String ratesUrl) {
    this.ratesUrl = ratesUrl;
    init(pullInterval);
  }

  public void init(Duration pullInterval) {
    ratesBasedOnRonCurrency.put(RON_CURRENCY,
      Currency.builder().currency(RON_CURRENCY).multiplier(BigDecimal.ONE).rate(BigDecimal.ONE).build());
    Flux.interval(Duration.ZERO, pullInterval)
      .onBackpressureLatest()
      .map(x -> getRates())
      .doOnNext(x -> updateRates(x))
      .subscribe();
  }

  private void updateRates(DataSet dataSet) {
    List<CurrencyDto> rates = dataSet.body.ratesSection.currencies;
    // 2020-10-29
    this.lastUpdate = LocalDate.parse(dataSet.body.ratesSection.date);
    rates.forEach(x -> {
      ratesBasedOnRonCurrency.computeIfAbsent(x.currencyName, (k) -> Currency.builder()
        .currency(x.currencyName)
        .multiplier(x.multiplier != null ? new BigDecimal(x.multiplier) : BigDecimal.ONE)
        .rate(new BigDecimal(x.rate))
        .build());
    });
    log.info("Rates updated. Size: {}", ratesBasedOnRonCurrency.size());
  }

  private DataSet getRates() {
    String content = "";
    DataSet data = null;
    try {
      configUnirestForUnsafeRequests();
      content = Unirest.get(ratesUrl)
        .asString()
        .getBody();
      data = parseXml(content);
    } catch (UnirestException | KeyManagementException | NoSuchAlgorithmException | KeyStoreException
        | JAXBException e) {
      throw new RuntimeException(e);
    }
    return data;
  }

  public static DataSet parseXml(String content) throws JAXBException {
    content = removeNamespaces(content);
    JAXBContext jaxbContext = findContext(DataSet.class);
    StringReader sr = new StringReader(content);
    return (DataSet) jaxbContext.createUnmarshaller().unmarshal(sr);
  }

  private static String removeNamespaces(String content) {
    return content.replace(
      "xmlns=\"http://www.bnr.ro/xsd\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.bnr.ro/xsd nbrfxrates.xsd\"",
      "");
  }

  private static void configUnirestForUnsafeRequests()
      throws NoSuchAlgorithmException, KeyManagementException, KeyStoreException {
    SSLContext sslContext = new org.apache.http.ssl.SSLContextBuilder()
      .loadTrustMaterial(null, new TrustSelfSignedStrategy()
        {
          public boolean isTrusted(X509Certificate[] chain, String authType) {
            return true;
          }
        })
      .build();
    HttpClient customHttpClient = HttpClients.custom()
      .setSSLContext(sslContext)
      .setSSLHostnameVerifier(new NoopHostnameVerifier())
      .build();
    Unirest.setHttpClient(customHttpClient);
  }

  private static JAXBContext findContext(Class<DataSet> clazz) {
    try {
      return JAXBContext.newInstance(clazz);
    } catch (JAXBException e) {
      e.printStackTrace();
    }
    return null;
  }

  public boolean areCurrenciesAvailable(CurrencyRequest currencyRequest) {
    return isCurrencyAvailable(currencyRequest.baseCurrency) && isCurrencyAvailable(currencyRequest.secondaryCurrency);
  }

  private boolean isCurrencyAvailable(String secondaryCurrency) {
    return ratesBasedOnRonCurrency.containsKey(secondaryCurrency);
  }

  public Currency getCurrency(String currency) {
    return ratesBasedOnRonCurrency.get(currency);
  }

}
