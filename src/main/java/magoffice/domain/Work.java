package magoffice.domain;

import java.time.OffsetDateTime;

import javax.persistence.AccessType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import lombok.AccessLevel;
import lombok.NonNull;
import lombok.extern.slf4j.Slf4j;

//@Value //TODO: add this to assure the object remains fully Immutable
//https://stackoverflow.com/questions/34241718/lombok-builder-and-jpa-default-constructor
@Slf4j
//Mandatory in conjunction with JPA: an equal based on fields is not desired
@lombok.EqualsAndHashCode(onlyExplicitlyIncluded = true)
//Mandatory in conjunction with JPA: force is needed to generate default values for final fields, that will be
//overriden by JPA
@lombok.NoArgsConstructor(access = AccessLevel.PRIVATE, force = true)
//Hides the constructor to force usage of the Builder.
@lombok.AllArgsConstructor(access = AccessLevel.PRIVATE)
@lombok.ToString
//Good to just modify some values
@lombok.With
//Mandatory in conjunction with JPA: Some suggest that the Builder should be above Entity -
//https://stackoverflow.com/a/52048267/99248
//Good to be used to modify all values
@lombok.Builder(toBuilder = true)
//final fields needed for imutability, the default access to public - since are final is safe
@lombok.experimental.FieldDefaults(makeFinal = true, level = AccessLevel.PUBLIC)
//no getters and setters
@lombok.Getter(value = AccessLevel.NONE)
@lombok.Setter(value = AccessLevel.NONE)

//JPA
@javax.persistence.Entity
@javax.persistence.Table(name = "WORK")
//jpa should use field access
@javax.persistence.Access(AccessType.FIELD)
public class Work {
  @lombok.EqualsAndHashCode.Include
  @Id
  @GeneratedValue
  Long id;

  @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE DEFAULT CURRENT_TIMESTAMP NOT NULL")
  @CreationTimestamp
  OffsetDateTime created;

  @Column(columnDefinition = "TIMESTAMP WITH TIME ZONE NOT NULL")
  @UpdateTimestamp
  OffsetDateTime updated;

  @NonNull
  String name;
  @NonNull
  String email;
  @NonNull
  String adresaLucrare;
  @NonNull
  String tipLucrare;
  boolean autorizatieContract;
  boolean contract;
}
