package magoffice.domain;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class Currency {
  public String currency;
  @Builder.Default
  public BigDecimal multiplier = BigDecimal.ONE;
  public BigDecimal rate;

  public BigDecimal multiplyRate() {
    return rate.multiply(multiplier);
  }
}
