package magoffice.ui.works;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.common.base.Preconditions;

import io.vavr.control.Either;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import magoffice.model.CurrencyInfoModel;
import magoffice.ui.common.CurrencyProviderService;
import magoffice.ui.common.RestResponse;
import magoffice.ui.rest.GenericController;

@Slf4j
@RestController
@RequestMapping("/api/currency/v1")
public class CurrencyProviderController extends GenericController {

  @Autowired
  private CurrencyProviderService currencyProviderService;

  @RequestMapping(method = RequestMethod.GET, path = "/{secondaryCurrency}")
  ResponseEntity<RestResponse<CurrencyInfoModel>> getCurrencies(@PathVariable(required = true) String secondaryCurrency,
      @RequestParam(required = false, name = "transform", defaultValue = "RON") String baseCurrency) {

    CurrencyRequest currencyRequest = CurrencyRequest.builder()
      .baseCurrency(baseCurrency)
      .secondaryCurrency(secondaryCurrency)
      .build();

    log.info("Requesting {}", currencyRequest);

    Either<CurrencyInfoModel, String> currency = currencyProviderService
      .retrieveParity(currencyRequest.validateAndFix());
    if (currency.isLeft()) {
      return ok("retrieved parity", currency.getLeft());
    } else {
      return error(HttpStatus.NOT_FOUND, currency.swap().getLeft());
    }

  }

  @Builder
  @Data
  public static class CurrencyRequest {
    private static final String ANY_LETTER_REGEX = "[a-zA-Z]+";
    public String baseCurrency;
    public String secondaryCurrency;

    public CurrencyRequest validateAndFix() {
      Preconditions.checkArgument(baseCurrency.matches(ANY_LETTER_REGEX),
        "Base currency must not contain other characters than letters");
      Preconditions.checkArgument(secondaryCurrency.matches(ANY_LETTER_REGEX),
        "Secondary currency must not contain other characters than letters");
      Preconditions.checkArgument(baseCurrency.length() == 3,
        "Base currency has wrong format. Example: [EUR] but was given: [" + baseCurrency + "]");
      Preconditions.checkArgument(secondaryCurrency.length() == 3,
        "Secondary currency has wrong format. Example: EUR but was given: [" + secondaryCurrency + "]");
      this.baseCurrency = baseCurrency.toUpperCase();
      this.secondaryCurrency = secondaryCurrency.toUpperCase();
      return this;
    }
  }
}
