package magoffice.ui.common;

import io.vavr.control.Either;
import magoffice.model.CurrencyInfoModel;
import magoffice.ui.works.CurrencyProviderController.CurrencyRequest;

public interface CurrencyProviderService {

  Either<CurrencyInfoModel, String> retrieveParity(CurrencyRequest currencyRequest);

}
