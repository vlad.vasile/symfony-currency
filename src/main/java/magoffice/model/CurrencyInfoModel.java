package magoffice.model;

import java.time.LocalDate;
import java.time.OffsetDateTime;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CurrencyInfoModel {
  public String parity;
  public String baseCurrency;
  public String secondaryCurrency;
  public int rounding;
  public OffsetDateTime requestDate;
  public LocalDate lastUpdate;

}
