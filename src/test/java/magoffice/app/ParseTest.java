package magoffice.app;

import javax.xml.bind.JAXBException;

import org.junit.jupiter.api.Test;
import org.raisercostin.jedio.Locations;

import magoffice.domain.CurrenciesFeed;
import magoffice.dto.AllData;
import magoffice.dto.DataSet;

public class ParseTest {

  @Test
  void testParsing() throws JAXBException {
    String content = Locations.classpath("static/data.xml").asReadableFile().readContent();
    DataSet allData = CurrenciesFeed.parseXml(content);
    System.out.println(allData);
  }
}
